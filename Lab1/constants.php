<?php
    $groups = array(1 => "PZ-11", 2 => "PZ-12", 3 => "PZ-13", 4 => "PZ-14", 5 => "PZ-15", 6 => "PZ-16", 7 => "PZ-17",
        8 => "PZ-18", 9 => "PZ-21", 10 => "PZ-22", 11 => "PZ-23", 12 => "PZ-24", 13 => "PZ-25", 14 => "PZ-26", 15 => "PZ-31",
        16 => "PZ-32", 17 => "PZ-33", 18 => "PZ-34", 19 => "PZ-35", 20 => "PZ-36", 21 => "PZ-41", 22 => "PZ-42", 23 => "PZ-43",
        24 => "PZ-44", 25 => "PZ-45", 26 => "PZ-46", 27 => "PZM-11", 28 => "PZIP-11", 29 => "PZIP-12",);
    $genders = array(1 => "Male", 2 => "Female", 3 => "Povtorka");
    define("USER_GROUPS", $groups);
    define("USER_GENDERS", $genders);
    define("DB_SERVER_NAME", "localhost");
    define("DB_USERNAME", "root");
    define("DB_PASSWORD", "");
    define("DB_NAME", "lab4db");
?>
