<!DOCTYPE html>
<html lang = "en">
<head>
    <meta charset = "UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"/>
    <link rel="stylesheet" href="styles/style.css"/>
    <link rel="manifest" href="/manifest.json">
    <title>Students</title>
</head>
<body>
    <?php
    require_once("constants.php");
    ?>
    <div class = "row bg-secondary p-3 d-flex justify-content-between m-0">
        <div class="col-1">
        <h2 class="text-light mb-0"><strong>CMS</strong></h2>
        </div>
        <div class="col-auto infoContainer pe-2 d-flex justify-content-end">
            <div class="drop msg-dropdown">
                <div class="notificationCircle">0</div>
                <a href=""><img class="bell" src="photos/bell.png" alt=""></a>
                    <div class="msg-dropdown-content">
                      <div class="msg">
                        <div class="msg-sender">
                          <img
                            class="msg-sender-img"
                            src="photos/bulgakoV_adekVatnyy.jfif"
                            alt="Avatar of user"
                          />
                          <p href="#">BulgakoV</p>
                        </div>
                        <span class="msg-text"></span>
                      </div>
                      <div class="msg">
                        <div class="msg-sender">
                          <img
                            class="msg-sender-img"
                            src="photos/otryady_putina.jpg"
                            alt="Avatar of user"
                          />
                          <p href="#">Babka</p>
                        </div>
                        <span class="msg-text"></span>
                      </div>
                    </div>
            </div>
            <div class="drop">
                <a href="https://getbootstrap.com/docs/5.3/components/navbar/#nav">
                    <img class="photo rounded-circle me-1" src="photos/sharii.jfif" alt="">
                    <div id="myModal1" class="modal">
                        <a href="#">Profile</a>
                        <a href="#">Log Out</a>
                    </div>
                <span class= "text-light form-label">Maksym Ferents</span>
                </a>
            </div>
        </div>
    </div>
    <div class="row mt-4 ms-0 me-0">
        <div class="col-2 justify-content-center d-flex">
            <div>
                <div class="dashboard"><a class="text-secondary" href="#">Dashboard</a></div>
                <div class="students"><a class="text-secondary font-weight-bold" href="#"><strong>Students</strong></a></div>
                <div class="tasks"><a class="text-secondary" href="#">Tasks</a></div>
            </div>
        </div>
        <div class="col-8">
            <div class="row">
            <h1 class="d-inline-block">Students</h1>
            </div>
            <div class="plusButton position-absolute row mt-3 mb-5">
            <button id = "buttonAddStudent" class = "btn40x40 btn border-dark border-2">
                <i class="fa-solid fa-plus"></i>
            </button>
            </div>

            <div class="row tableOfStudents">
            <table id = "table">
                <tbody class="text-center">
                <tr class="border-0">                   
                    <th class="col-1"><span></span></th>
                    <th class="col"><span class="headerOfTable">Group</span></th>
                    <th class="col"><span class="headerOfTable">Name</span></th>
                    <th class="col"><span class="headerOfTable">Gender</span></th>
                    <th class="col"><span class="headerOfTable">Birthday</span></th>
                    <th class="col"><span class="headerOfTable">Status</span></th>
                    <th class="col"><span class="headerOfTable">Options</span></th>
                </tr>
                <?php
                $conn = new mysqli(DB_SERVER_NAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
                $tablename = "students";
                $sql = "SELECT * FROM $tablename";
                $result = $conn->query($sql);
                $students = array();
                while ($student = $result->fetch_assoc()) {
                    ?>
                    <tr data-id = "<?= $student["id"]?>">
                        <td class="col"><span><input type="checkbox"></span></td>
                        <td class="col" data-id="<?= $student["group_id"]?>"><span><?= USER_GROUPS[$student["group_id"]]?></span></td>
                        <td class="col"><span><?= $student["name"]." ".$student["surname"]?></span></td>
                        <td class="col" data-id="<?= $student["gender_id"]?>"><span><?= USER_GENDERS[$student["gender_id"]]?></span></td>
                        <td class="col"><span><?= $student["birthday"]?></span></td>
                        <td class="col"><span class="circle d-inline-block"></span></td>
                        <td class="col">
                        <span class="buttonContainer d-flex justify-content-center">
                            <button class="tableButton p-0 editButton"><i class="fa-solid fa-pen"></i></button>
                            <button class="tableButton p-0 deleteButton"><i class="fa-solid fa-xmark"></i></button>
                        </span>
                        </td>
                    </tr>
                    <?php
                }
                $conn->close();
                ?>

                </tbody>
            </table>
            </div>
        </div>
        </div>
        <div class="student-window">
            <div class="student-window-content">
              <header class="student-window-header">
                <h3 class="student-window-heading">Add student</h3>
                <button class="btn--close-student-window"><i class="fa-sharp fa-solid fa-xmark"></i></button>
              </header>
              <form id="student-form" class="student-form" method="post" action="server.php">
                <div class="student-input-container">
                  <label for="group_id" class="student-form-label">Group</label>
                  <select name="group_id" id="group_id" class="student-input" required>
                      <option value="0" selected>Select group</option>
                      <?php
                      foreach (USER_GROUPS as $key => $group){
                          ?>
                          <option value="<?= $key ?>"><?= $group ?></option>
                          <?php
                      }
                      ?>
                  </select>
                    <label for="group" class="text-danger group-error student-form-label">Invalid group selected</label>
                </div>
                <div class="student-input-container">
                  <label for="name" class="student-form-label"
                    >Name</label>
                  <input
                    Name="name"
                    id="name"
                    type="text"
                    class="student-input"
                    required
                  />
                    <label for="name" class="text-danger name-error student-form-label">Invalid name</label>
                </div>
                <div class="student-input-container">
                  <label for="surname" class="student-form-label"
                    >Surname</label>
                  <input
                    surname="surname"
                    id="surname"
                    type="text"
                    class="student-input"
                    required
                  />
                    <label for="surname" class="text-danger surname-error student-form-label">Invalid surname</label>
                </div>
                <div class="student-input-container">
                  <label for="gender" class="student-form-label">Gender</label>
                  <select
                    name="gender_id"
                    id="gender_id"
                    class="student-input student-form-label"
                    required
                  >
                    <option value="0">Select gender</option>
                      <?php
                        foreach (USER_GENDERS as $key => $gender){
                            ?>
                            <option value="<?= $key ?>"><?= $gender ?></option>
                            <?php
                        }
                        ?>
                  </select>
                    <label for="gender_id" class="text-danger gender_id-error student-form-label">Invalid gender selected</label>
                </div>
                <div class="student-input-container">
                  <label for="birthday" class="student-form-label"
                    >Birthday</label
                  >
                  <input
                    name="birthday"
                    id="birthday"
                    type="date"
                    class="student-input"
                    required
                  />
                    <label for="birthday" class="text-danger birthday-error">Invalid birthday selected</label>
                </div>
                <div class="form-buttons">
                  <button class="btn--form btn--form-submit" type="submit">
                    Ok
                  </button>
                  <button class="btn--form btn--form-cancel" type="button">
                    Cancel
                  </button>
                </div>
              </form>
            </div>
          </div>
          <div class="delete-student-window">
            <div class="delete-student-window-content">
              <header class="delete-student-header">
                <h3 class="delete-student-window-heading">Warning</h3>
                <button class="btn--close-student-window"><i class="fa-sharp fa-solid fa-xmark"></i></button>
              </header>
              <p class="delete-student-text">
                Are you sure you want to delete
                <span class="student-to-delete">User</span>?
              </p>
              <form class="delete student-form" method="post" action="server.php">
                <div class="form-buttons">
                  <button
                    class="btn--form btn--delete-student-submit"
                    type="submit">Ok
                  </button>
                  <button
                    class="btn--form btn--delete-student-cancel"
                    type="button">Cancel
                  </button>
                </div>
              </form>
            </div>
          </div>
  
        <script src="scripts/Lab1.js"></script>
    <ul class="pagination">
        <li><i class="fa-solid fa-arrow-left"></i></li>
        <li class="Font"><a href="#">1</a></li>
        <li class="Font"><a href="#">2</a></li>
        <li class="Font"><a href="#">3</a></li>
        <li><i class="fa-solid fa-arrow-right"></i></li>
    </ul>
    </body>
</html>
