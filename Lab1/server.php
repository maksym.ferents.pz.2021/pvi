<?php
require_once('constants.php');
$tablename = "students";
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!empty($_POST["name"])) {
        $group = $_POST["group"];
        $name = $_POST["name"];
        $surname = $_POST["surname"];
        $gender = $_POST["gender"];
        $birthday = $_POST["birthday"];
        $id = $_POST["id"];

        $errors = new stdClass();
            if (empty($group)) {
                $errors -> errorText = "Empty group";
            }

            if (empty($name)) {
                $errors -> errorText = "Empty first name";
            }
            if(empty($surname)){
                $errors -> errorText = "Empty last name";
            }

            if (empty($gender)) {
                $errors -> errorText = "Empty gender";
            } elseif($gender != "1" && $gender != "2" && $gender != "3") {
                $errors -> errorText = "Wrong gender value";
            }

            if (empty($birthday)) {
                $errors -> errorText = "Empty birthday";
            }

            $response = new stdClass();

            $student = new stdClass();
            $student -> group = $group;
            $student -> name = $name." ".$surname;
            $student -> gender = $gender;
            $student -> birthday = $birthday;
            $conn = new mysqli(DB_SERVER_NAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
        if($id == -1) {
            $conn -> begin_transaction();
            $sql = 'SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA="lab4db" AND TABLE_NAME="students"';
            $id = $conn -> query($sql) -> fetch_assoc()["AUTO_INCREMENT"];
            $student -> id = $id;
            $sql = "INSERT INTO $tablename (group_id, name, surname, gender_id, birthday) VALUES ('$group', '$name', '$surname' ,'$gender', '$birthday')";
            if (!($conn->query($sql) === TRUE)) {
                $errors -> errorText = "Student wasn't added: " . $conn->error . "\n";
                $conn -> rollback();
            }else {
                $conn->commit();
            }
        } else {
            $sql = "UPDATE $tablename SET group_id='$group', name='$name', surname='$surname', gender_id='$gender', birthday='$birthday' WHERE id='$id'";
            if (!($conn->query($sql) === TRUE)) {
                $errors -> errorText = "Student wasn't edited: " . $conn->error . "\n";
            }
        }
            $conn->close();
            $response -> student = $student;
            if(!empty($errors -> errorText)) {
                $response -> error = $errors;
                $response -> status = 0;
            }else{
                $response -> error = null;
                $response -> status = 1;
            }
            header('Content-Type: application/json');

            echo json_encode($response);
            exit;
        } else{
        $id = $_POST["id"];
        $conn = new mysqli(DB_SERVER_NAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
        $errors = new stdClass();
        if ($conn->connect_error) {
            $errors -> errorText = "Connection failed: " . $conn->connect_error . "\n";
        }
        $sql = "DELETE FROM $tablename WHERE id='$id'";

        if (!($conn->query($sql) === TRUE)) {
            $errors -> errorText = "Student wasn't deleted: " . $conn->error . "\n";
            $response = array('status' => 0, 'errors' => $errors);
        } else {
            $response = array('status' => 1);
        }
            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        }
    }
