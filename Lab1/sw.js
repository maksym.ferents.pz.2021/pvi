const assets = [
    '/',
    '/index.php',
    '/styles/style.css',
    '/scripts/Lab1.js',
    'photos/admin_photo.webp', 
    'photos/bell.png',
    'photos/bulgakoV_adekVatnyy.jfif',
    'photos/otryady_putina.jpg', 
    'photos/sharii.jfif'
  ]

self.addEventListener("install", installEvent => {
    installEvent.waitUntil(
    caches.open("pwa-assets").then(cache => {
        cache.addAll(assets)
    })
    )
})

self.addEventListener("fetch", fetchEvent => {
    fetchEvent.respondWith(
      caches.match(fetchEvent.request).then(res => {
        return res || fetch(fetchEvent.request)
      })
    )
  })
